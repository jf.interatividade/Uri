
import java.io.IOException;
import java.util.Scanner;
 
public class Main {
    public static void main(String[] args) throws IOException {
    double raio,area;
    Scanner inp = new Scanner(System.in);
    raio = inp.nextInt();
    area = raio*raio * 3.14159;
    System.out.printf("A=%0.4f",area);
    }
 
}